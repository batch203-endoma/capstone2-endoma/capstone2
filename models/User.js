const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "is required"]
  },
  lastName: {
    type: String,
    required: [true, "Last Name is required"]
  },
  email: {
    type: String,
    required: [true, "Last Name is required"]
  },
  password: {
    type: String,
    required: [true, "Password is required"]
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile Number is required"]
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  orders: [{
    totalAmount: {
      type: Number
    },
    purchasedOn: {
      type: Date,
      default: new Date()
    },
    products: [{
      productId: {
        type: String,
        required: [true, "Product ID is required"]
      },
      productName: {
        type: String,
        required: [true, "Product name is required"]
      },
      quantity: {
        type: Number,
        required: [true, "Product quantity is required"]
      }
    }]
  }]
})



module.exports = mongoose.model("User", userSchema);
// // User
// firstName - string,
//   lastName - string,
//   email - string,
//   password - string,
//   mobileNo - string,
//   isAdmin - boolean,
//   default: false
// orders: [
//
//   {
//     totalAmount - number,
//     purchasedOn - date
//     default: new Date(),
//     products - [
//
//       {
//         productId - string,
//         productName - optional
//         quantity - number
//       }
//
//     ]
//   }
//
// ]

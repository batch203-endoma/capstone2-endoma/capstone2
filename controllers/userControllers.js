const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

//Get all users

module.exports.getAllUsers = async (req, res) => {
  const userData = await auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return User.find({}).then((users) => res.send(users));
  } else {
    res.send(`You're not authorized to access this page.`);
  }
};

//register user
module.exports.registerUser = async (req, res) => {
  //console.log(req.body);

  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    address: req.body.address,
    password: bcrypt.hashSync(req.body.password, 10),
    mobileNo: req.body.mobileNo,
    // orders:[{
    //   products:""
    // }]
  });
  //new user information

  let emailCheck = await User.find({
    email: req.body.email,
  })
    .then((result) => result.length)
    .catch((err) => res.send(err));
  console.log(emailCheck);

  if (emailCheck > 0) {
    res.send(false);
  } else {
    newUser
      .save()
      .then((registeredUser) => {
        console.log(registeredUser);
        res.send(true);
      })
      .catch((err) => {
        console.log(err);
        res.send(false);
      });
  }
};

//User log in
module.exports.loginUser = (req, res) => {
  return User.findOne({
    email: req.body.email,
  }).then((result) => {
    //user does not exists
    if (result == null) {
      // return res.send(false);
      return res.send(false);
    }
    //user exists
    else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );
      if (isPasswordCorrect) {
        return res.send({
          message: `Log in Successful, Welcome!`,
          accessToken: auth.createAccessToken(result),
        });
      } else {
        return res.send({
          message: `Invalid Credentials`,
        });
      }
    }
  });
};

//User Order
module.exports.createOrder = async (req, res) => {
  const userData = await auth.decode(req.headers.authorization);

  for (let i = 0; i < req.body.products.length; i++) {
    let productInformation = await Product.findById(
      req.body.products[i].productId
    ).then((result) => {
      console.log(result);
      return result;
    });

    let data1 = {
      userId: userData.id,
      email: userData.email,
      price: productInformation.price,
      productId: req.body.products[i].productId,
      quantity: req.body.products[i].quantity,
      productName: productInformation.productName,
    };

    let totalPrice = data1.price * data1.quantity;

    if (userData.isAdmin) {
      res.send(false);
    } else if (productInformation.stock < req.body.products[i].quantity) {
      res.send(false);
    } else {
      // console.log(data)
      // let totalAmount = await (productInformation.price * data.quantity);

      let updateUserOrder = await User.findById(userData.id).then((user) => {
        user.orders.push({
          // products: req.body.products,
          totalAmount: totalPrice,
          products: {
            productName: data1.productName,
            productId: data1.productId,
            quantity: data1.quantity,
            // totalAmount: totalAmount,
            // products: {
            // productName: req.body.productName,

            // }
          },
        });
        return user
          .save()
          .then((result) => {
            console.log(result);
            return true;
          })
          .catch((err) => {
            console.log(err);
            return false;
          });
      });

      let data = {
        userId: userData.id,
        email: userData.email,
        // price: productInformation.price,
        productId: req.body.products[i].productId,
        quantity: req.body.products[i].quantity,
        productName: req.body.products[i].productName,
      };

      let isProductUpdated = await Product.findById(data.productId).then(
        (product) => {
          product.orders.push({
            userId: data.userId,
            email: data.email,
            quantity: data.quantity,
          });
          product.stock -= data.quantity;
          // res.send(product);
          return product
            .save()
            .then((result) => {
              console.log(result);
              return true;
            })
            .catch((err) => {
              console.log(err);
              return false;
            });
        }
      );

      // let findUser = await User.findById(data.userId)
      //   .then(result => {
      //     return Object.values(result)
      //   })
      //
      //
      // let myAmount = Object.values(findUser)
      // console.log(myAmount);

      // const values = Object.values(userData.orders);
      //
      // const sum = values.reduce((accumulator, value) => {
      //   return accumulator + value;
      // }, 0)
      // console.log(sum)
      // let productPurchased = [];
      // productPurchased.push(data1.productName)
      if (
        updateUserOrder &&
        isProductUpdated &&
        i == [req.body.products.length - 1]
      ) {
        // console.log(productPurchased);
        res.send(true);
      } else if (i !== [req.body.products.length - 1]) {
        console.log(false);
      } else {
        res.send(false);
      }
      // console.log(i)
      // await (updateUserOrder && isProductUpdated && i <= 1) ? res.send(true): res.send(false)
      // await (updateUserOrder && isProductUpdated && i == [req.body.products.length-1] ) ? res.send(`Thank you for shopping with us! You've purchased ${req.body.products[i].productName} - ${req.body.quantity} pcs with a total amount of Total amount insert<.`): console.log(false)
      // res.send(true)
    }
  }
};

// const values = Object.values(userData.orders);
//
// const sum = values.reduce((accumulator, value) => {
//   return accumulator + value;
// }, 0)
// var numbers = userData.orders
// let grandTotalAmount = numbers.forEach(function(numbers) {
//   return totalAmount + numbers;
// })
//
//
// console.log(Number(userData.orders.totalAmount) + Number(totalAmount));
// console.log(grandTotalAmount + `here`);

//getProfile
module.exports.getProfile = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  return User.findById(userData.id, {
    orders: 0,
  }).then((result) => {
    result.password = "*";
    res.send(result);
  });
};
//Stretch Goals
// 1. set user as admin()

module.exports.setUserToAdmin = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let setToAdmin = {
    isAdmin: req.body.isAdmin,
  };

  let getUserRole = await User.findById(req.params.userId).then(
    (result) => result.isAdmin
  );

  if (userData.isAdmin) {
    if (getUserRole !== true) {
      return User.findByIdAndUpdate(req.params.userId, setToAdmin, {
        new: true,
      })
        .then((result) => {
          console.log(result);
          res.send(true);
        })
        .catch((err) => {
          console.log(err);
          res.send(false);
        });
    } else {
      return res.send(false);
    }
  } else {
    return res.status(401).send(false);
  }
};

module.exports.getOrders = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return User.find(
      {},
      {
        email: 1,
        orders: 1,
        _id: 1,
      }
    ).then((result) => {
      res.send(result);
    });
  } else {
    // console.log(userData);
    return User.findById(userData.id, {
      orders: 1,
      _id: 0,
    }).then((result) => {
      res.send(result);
    });
  }
};

//

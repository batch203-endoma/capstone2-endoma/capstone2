const jwt = require("jsonwebtoken");
const secret = "ecommerceAPI";


//Payload
module.exports.createAccessToken = (user) => {
  console.log(user)
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
    // orders: [{
    //   totalAmount: 0
    // }]
  }
  return jwt.sign(data, secret, {});
}

// change payload.

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (token !== undefined) {
    token = token.slice(7, token.length);
    console.log(token);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({
          auth: "Invalid token!"
        });
      } else {
        next();
      }
    })
  } else {
    res.send({
      message: "Auth Failed. No token provided!"
    })

  }
}



module.exports.decode = (token) => {
  if (token !== undefined) {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, {
          complete: true
        }).payload;
      }
    })
  } else {
    return null
  }
}

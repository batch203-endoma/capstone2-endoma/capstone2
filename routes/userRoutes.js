const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

console.log(userControllers);

//Get all users (test)
router.get("/", auth.verify, userControllers.getAllUsers);

//Router for registration
router.post("/register", userControllers.registerUser);

//Route for user authentication
router.post("/login", userControllers.loginUser);

//Route for creating Order
router.post("/order", auth.verify, userControllers.createOrder);

router.get("/details", auth.verify, userControllers.getProfile);

router.patch("/:userId", auth.verify, userControllers.setUserToAdmin);

module.exports = router;

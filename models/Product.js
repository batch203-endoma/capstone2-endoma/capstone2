const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product name is required"],
  },
  description: {
    type: String,
    required: [true, "Product description is required"],
  },
  price: {
    type: Number,
    required: [true, "Product price is required"],
  },
  productCode: {
    type: String,
    required: [true, "Product code is required"],
  },
  category: {
    type: String,
    required: [true, "Category is required"],
  },
  image: {
    type: String,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  stock: {
    type: Number,
    required: [true, "Product quantity is required"],
  },
  orders: [
    {
      orderId: {
        type: String,
      },
      userId: {
        type: String,
        required: [true, "user id is required"],
      },
      userEmail: {
        type: String,
      },
      quantity: {
        type: Number,
        required: [true, "Product quantity is required"],
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

// Product
// name - string,
//   description - string,
//   price - number
// isActive - boolean
// default: true,
// createdOn - date
// default: new Date()
// orders: [
//
//   {
//     orderId - string,
//     userId - string,
//     userEmail - optional,
//     quantity - number,
//     purchasedOn - date
//     default: new Date(),
//   }
//
// ]

module.exports = mongoose.model("Product", productSchema);

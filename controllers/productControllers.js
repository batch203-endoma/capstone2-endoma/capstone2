const Product = require("../models/Product.js");
const auth = require("../auth.js");
const User = require("../models/User.js");

// Add/Create newProduct(admin only)
module.exports.createProductListing = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let addNewProduct = new Product({
    productName: req.body.productName,
    description: req.body.description,
    price: req.body.price,
    stock: req.body.stock,
    productCode: req.body.productCode,
    category: req.body.category,
    image: req.body.image,
  });

  let productCheck = await Product.find({
    productCode: req.body.productCode,
  })
    .then((count) => count.length)
    .catch((err) => res.send(err));

  if (userData.isAdmin !== true) {
    res.send(false);
  } else {
    if (productCheck > 0) {
      return res.send(false);
    } else {
      addNewProduct
        .save()
        .then((product) => {
          res.send(true);
        })
        .catch((err) => {
          res.send(false);
        });
    }
  }
};

// Retrieve all Active Products.
module.exports.retrieveAllActiveProducts = (req, res) => {
  return Product.find({
    isActive: true,
  }).then((result) => res.send(result));
};

//Retrieve specific product
module.exports.getProduct = (req, res) => {
  console.log(req.params.productId);
  return Product.findById(req.params.productId).then((result) =>
    res.send(result)
  );
};

//updating/modify specific product
module.exports.updateProduct = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    let updateProduct = {
      productName: req.body.productName,
      description: req.body.description,
      price: req.body.price,
      stock: req.body.stock,
      category: req.body.category,
      image: req.body.image,
    };
    return Product.findByIdAndUpdate(req.params.productId, updateProduct, {
      new: true,
    }).then((result) => {
      console.log(result);
      res.send(true);
    });
  } else {
    return res.status(401).send(false);
  }
};

//Archiving of specific product
module.exports.archiveProduct = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let updateIsActiveField = {
    isActive: req.body.isActive,
  };
  if (userData.isAdmin) {
    return Product.findByIdAndUpdate(
      req.params.productId,
      updateIsActiveField,
      {
        new: true,
      }
    )
      .then((result) => {
        console.log(result);
        res.send(true);
      })
      .catch((err) => {
        console.log(err);
        res.send(false);
      });
  } else {
    return res.status(401).send(false);
  }
};

//Retrieve all Active products w/ stock
module.exports.getAllProductsAdmin = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return Product.find(
      {},
      {
        orders: 0,
      }
    ).then((result) => res.send(result));
  } else {
    res.send(false);
  }
};

module.exports.getAllOrders = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    return await Product.find(
      {},
      {
        orders: 1,
      }
    ).then((result) => res.send(result));
  } else {
    res.send(false);
  }
};
//

const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

console.log(productControllers);

//create a product (Admin Only);

router.post("/", auth.verify, productControllers.createProductListing);

//Retrieve all Active products
router.get("/", productControllers.retrieveAllActiveProducts);

//Get all orders
router.get("/getAllOrders", auth.verify, productControllers.getAllOrders);

//Retrieve all Active admin
router.get(
  "/getProductsAdmin",
  auth.verify,
  productControllers.getAllProductsAdmin
);
//Retrieve specific product
router.get("/:productId", productControllers.getProduct);

//Route for updating a course
router.put("/:productId", auth.verify, productControllers.updateProduct);

//Archiving Product
router.patch("/:productId", auth.verify, productControllers.archiveProduct);

module.exports = router;

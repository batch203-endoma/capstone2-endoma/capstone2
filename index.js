const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js")
const productRoutes = require("./routes/productRoutes.js")
const app = express();


mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.wvosk1v.mongodb.net/b203_e-commerce_02?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

const port = process.env.PORT || 4000;

//MiddleWare
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);


//listen
app.listen(port, () => {
  console.log(`API is now online on port ${port}`)
})

// // Model Sketch for the E-Commerce API
//
// // 2-Model Structure (Two way embedding)
//
// // User
// firstName - string,
//   lastName - string,
//   email - string,
//   password - string,
//   mobileNo - string,
//   isAdmin - boolean,
//   default: false
// orders: [
//
//   {
//     totalAmount - number,
//     purchasedOn - date
//     default: new Date(),
//     products - [
//
//       {
//         productId - string,
//         productName - optional
//         quantity - number
//       }
//
//     ]
//   }
//
// ]
//
// // Product
// name - string,
//   description - string,
//   price - number
// isActive - boolean
// default: true,
// createdOn - date
// default: new Date()
// orders: [
//
//   {
//     orderId - string,
//     userId - string,
//     userEmail - optional,
//     quantity - number,
//     purchasedOn - date
//     default: new Date(),
//   }
//
// ]
//
// // 3-Model Structure (Referencing)
//
// // // User
// // firstName - string,
// //   lastName - string,
// //   email - string,
// //   password - string,
// //   mobileNo - string,
// //   isAdmin - boolean,
// //   default: false
// //
// //
// // // Product
// // name - string,
// //   description - string,
// //   price - number
// // isActive - boolean
// // default: true,
// // createdOn - date
// // default: new Date()
// //
// // // Order
// // totalAmount - number,
// //   purchasedOn - date
// // default: new Date(),
// //   userId - string
// // products - [
// //
// //   {
// //     productId - string,
// //     quantity - number
// //   }
// //
// // ]

const mongoose = require("mongoose");

const bagSchema = new mongoose.Schema({
  totalAmount: {
    type: Number,
  },
  userId: {
    type: String,
    required: [true, "is required"],
  },
  products: [
    {
      productId: {
        type: String,
      },
      quantity: {
        type: Number,
      },
    },
  ],
});

module.exports = mongoose.model("Product", productSchema);
// totalAmount - number,
// //   purchasedOn - date
// // default: new Date(),
// //   userId - string
// // products - [
// //
// //   {
// //     productId - string,
// //     quantity - number
// //   }
// //
// // ]
